@extends('layouts.backend.backendDesign')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 mt-3 shadow">
                    <div class="col-sm-6">
                        <h1 class="text-bold">Update Record-Data Center </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active text-bold">Update Record</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content mt-5">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <!-- left column -->

                    <div class="col-md-10">
                        @if (Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{!! session('error') !!}</strong>
                            </div>
                        @endif
                        @if (Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{!! session('success') !!}</strong>
                            </div>
                        @endif
                        <br>

                        <!-- Horizontal Form -->
                        <div class="card card-info">
                            <div class="card-header">
                                <button class="btn btn-dark shadow" style="float: right">Update Record</button>
                            </div>

                            <!-- /.card-header -->
                            <!-- form start -->
                            <form action="{{route('update.record',$record->id)}}" enctype="multipart/form-data" class="form-horizontal" name="updateRecord" id="updateRecord" method="post" >
                                {{csrf_field()}}
                                <div class="card-body">
                                    <div class="form-group row ">
                                        <label for="reference_number" class="col-sm-4 col-form-label">Reference Number</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="reference_number" id="reference_number" value="{{$record->reference_number}}" >
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="" class="col-sm-4 col-form-label">Receiver</label>
                                        <div class="col-sm-8">
                                            <select name="parent_id"  class="form-control" required>
                                                <option value="0">Receiver Name</option>
                                              @foreach(App\User::all() as $user )
                                                <option value="{{$user->id}}"
                                                @if($user->id == $recordDetails->parent_id) selected @endif > {{$user->name}}
                                                </option>
                                              @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="senderName" class="col-sm-4 col-form-label">Sender</label>
                                        <div class="col-sm-8">
                                            <select name="sender" class="form-control" id="sender" required>
                                                <option value="Admin">Admin</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="subject" class="col-sm-4 col-form-label">Subject</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="subject" id="subject" value="{{$record->subject}}" required >
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="subject" class="col-sm-4 col-form-label">Date</label>
                                        <div class="col-sm-8">
                                            <input type="date" class="form-control" name="date" id="date" value="{{$record->date}}"  required >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="file" class="col-sm-4 col-form-label">File Upload</label>
                                        <div class="form-group col-sm-8 ">
                                            <input type="file" class="form-control-file" name="file" id="file">

                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="mb-3 card-footer row justify-content-center" >
                                    <button type="submit" class="btn btn-info">Update Data</button>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@stop


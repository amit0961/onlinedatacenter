@extends('layouts.backend.backendDesign')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2  mt-3 shadow">
                    <div class="col-sm-6">
                        <h1 class="text-bold" >View Record Data</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active text-bold">View Record</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @if (Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{!! session('success') !!}</strong>
                        </div>
                    @endif

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">All the Records are here....</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-hover table-bordered table-striped">
                                <thead>
                                <tr class="text-center">
                                    <th>Record ID</th>
                                    <th>Reference No.</th>
                                    <th>Receiver Name</th>
                                    <th>Sender Name</th>
                                    <th>Subject</th>
                                    <th>Date</th>
                                    <th>File</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($records as $record)
                                    <tr>
                                        <td>{{$record->id}}</td>
                                        <td>{{$record->reference_number}}</td>
                                        <td>{{$record->user->name}}</td>
                                        <td>{{$record->sender}}</td>
                                        <td>{{$record->subject}}</td>
                                        <td> {{date('F d, Y', strtotime($record->date))}}</td>
                                        <td>
                                           @if(empty($record->file))
                                                <p>Please Up</p>
                                            @else
                                                <a href="{{Storage::url($record->file)}}">Download</a>
                                            @endif
                                        </td>
                                        <td class="row justify-content-center">
                                            <!-- <a href="#myModal {{$record->id}}" data-target="#myModal{{$record->id}}  " data-toggle="modal" class="btn btn-outline-success btn-sm">View</a> -->
                                           <a href="{{route('edit.record',$record->id)}}" id="editCat" class="btn btn-outline-primary btn-sm">Edit</a>
                                            <a href="{{route('delete.record',$record->id)}}" class="btn btn-outline-danger btn-sm">Delete</a>
                                        </td>
                                    </tr>

                               @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@stop

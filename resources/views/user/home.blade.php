


@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-md-2">
                <div class="btn-group">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <h3 style="font-weight: bold" > <span class="shadow">Online</span>  <br> <span class="shadow">Mail-Box</span>  <br> <span class="shadow">System</span>  </h3>
                </a>
                </div>
            </div>
            <div class="col-sm-9 col-md-10">
                <div class="btn-group">
                    <form action="{{route('searchRecord')}}" method="get">
                        <div class="form-inline">
                            <div class="form-group">
                                <select  placeholder="Name" name="parent_id" class="form-control">
                                    <option>Receiver</option>
                                    @foreach(App\User::all() as $user)
                                    <option value="{{$user->id}}" >{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>&nbsp; &nbsp;
                            <div class="form-group">
                                <input  placeholder="Date" type="date" name="date" class="form-control">
                            </div>&nbsp;
                            <div class="form-group">
                                <button type="submit" class="btn btn-sm btn-outline-dark">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-sm-3 col-md-2">
                <a href="#" class="btn btn-danger btn-sm btn-block" role="button"><i class="fa fa-user" aria-hidden="true"></i> Receiver Inbox</a>
                    <hr />
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active">
                        </li>
                    </ul>
            </div>
            <div class="col-sm-9 col-md-10">
                            <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li ><span  style="text-decoration:underline; color:green; font-size: 16px; font-weight:bold;"><i class="fa fa-envelope" aria-hidden="true"></i>
Primary Inbox</span></li>
                </ul>
                <table class="table table-hover" style="width: 100%">
                    <tbody>
                        @foreach($records as $record)
                            <tr>
                                <td style=" width: 40% ; ">
                                    <span >[ref:{{$record->reference_number}} ]</span>-
                                    <span style="font-weight:bold; font-size: 16px; " >{{$record->sender}} <i class="fa fa-tags" aria-hidden="true"></i></span>
                                </td>
                                <td style="width: 35%">
                                    <span style="font-size: 16px; font-weight:bold;">{{$record->user->name}}</span>
                                    <span class="text-muted" style="font-size: 13px;">- {{$record->subject}} </span>
                                </td>
                                <td style="width: 5%">
                                    <span  style="font-size:11px"><i class="fa fa-file" aria-hidden="true"></i>{{$record->file}}</span>
                                </td>
                                <td style="width: 5%">
                                    <span class="badge" style="font-size:11px"><i class="fa fa-clock-o" aria-hidden="true"></i> {{date('F d, Y', strtotime($record->date))}}</span>
                                </td>
                                <td style="width: 5%" >
                                @if(empty($record->file))
                                    <p>No File</p>
                                @else
                                    @if(Auth::user())
                                        <a href="public\files/{{$record->file}}">
                                            <button class="btn btn-sm btn-outline-dark" >Download Now <i class="fa fa-download" aria-hidden="true"></i></button>
                                        </a>
                                    @else
                                        <a href="{{route('userloginform')}}">
                                            <button class="btn btn-sm btn-outline-dark">Login to Download <i class="fa fa-download" aria-hidden="true"></i></button>
                                        </a>
                                    @endif
                                @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>           
            </div>
        </div>
    </div>
    
@endsection


$(document).ready(function(){




	$('input[type=checkbox],input[type=radio],input[type=file]').uniform();

	$('select').select2();




	// Form Validation
    $("#basic_validate").validate({
		rules:{
			required:{
				required:true
			},
			email:{
				required:true,
				email: true
			},
			date:{
				required:true,
				date: true
			},
			url:{
				required:true,
				url: true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});
  // Form Validation End




	$("#number_validate").validate({
		rules:{
			min:{
				required: true,
				min:10
			},
			max:{
				required:true,
				max:24
			},
			number:{
				required:true,
				number:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});


});




 //...................................password validate & check ......................
 $(document).ready(function(){

    //password check
    $("#current_pwd").keyup(function(){
        var current_pwd = $("#current_pwd").val();
        $.ajax({
            type:'get',
            url:'/admin/check-pwd',
            data:{current_pwd:current_pwd},
            success:function(resp){
                //alert(resp);
                if(resp=="false"){
                    $("#chkPwd").html("<font color='red'>Current Password is Incorrect</font>");
                }else if(resp=="true"){
                    $("#chkPwd").html("<font color='green'>Current Password is Correct</font>");
                }
            },error:function(){
                alert("Error");
            }
        });
    });
        //password check End

          //password validate
    $("#password_validate").validate({
        rules:{
            current_pwd:{
                required: true,
                minlength:6,
                maxlength:20
            },
            new_pwd:{
                required: true,
                minlength:6,
                maxlength:20
            },
            confirm_pwd:{
                required:true,
                minlength:6,
                maxlength:20,
                equalTo:"#new_pwd"
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });
    //password validate End

});


//...................................password check..................................














//...................................Add Correspondence Validation......................


$(document).ready(function(){
$("#add_correspondence").validate({
    rules:{
        reference_number:{
            required:true,

        },
        parent_id:{
            required:true,

        },
        sender:{
            required:true,

        },
        subject:{
            required:true,
        },

        date:{
            required:true,


        },
         file:{
           required:true,

         }



    },
    errorClass: "help-inline",
    errorElement: "span",
    highlight:function(element, errorClass, validClass) {
        $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents('.control-group').removeClass('error');
        $(element).parents('.control-group').addClass('success');
    }
});
});
//................................... Add Correspondence Validation End......................



//...................................Edit Correspondence Validation ........................
$(document).ready(function(){
    $("#edit_correspondence").validate({
        rules:{
            reference_number:{
                required:true,

            },
            parent_id:{
                required:true,

            },
            sender:{
                required:true,

            },
            subject:{
                required:true,
            },

            date:{
                required:true,


            },
            file:{
                required:true,

            }



        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });
});
//...................................Edit Correspondence Validation ......................



//...................................delete Correspondence confirmation...................

$(document).ready(function(){
$("#delcat").click(function(){
    // alert("test");
    if(confirm('Are you sure want to Delete this ?')) {

        return true;
    }
    return false

});
});
//...................................delete Correspondence confirmation......................





//...................................Delete Correspondence By sweetAlert.........................................

$(document).on('click','.deleteRecord',function(e){
    var id = $(this).attr('rel');
    var deleteFunction = $(this).attr('rel1');
    swal({
            title: "Are you sure?",
            text: "Remember!  You will not be able to recover this Record Again! OK?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function(){
           // window.location.href="/admin/"+deleteFunction+"/"+id;
            window.location.href="/admin/"+id+"/"+deleteFunction;
        });
});

//...................................Delete Correspondence By sweetAlert End...........................



<?php

namespace App\Http\Controllers;
use App\Admin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;



class AdminController extends Controller
{
    public function adminregistration(){
        $user= User::create([
            'name'      => request('name'),
            'email'     => request('email'),
            'user_type' => request('user_type'),
            'password'  => Hash::make(request('password')),
        ]);
        Admin::create([
            'username'  => request('username'),
            'password'  => Hash::make(request('password')),
        ]);
        return redirect()->back();
    }

    public function loginform(){
        return view('backend.adminLogin');
    }

    public function adminLogin(Request $request){
        if ($request->ismethod('post')) {
            $data = $request->input();
            if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'],'user_type'=>'admin', 'admin' => '1'])) {
                return view('backend.dashboard');
            } else {
                return back()->with('error',' Invalid UserName Or Password');;
            }
        }
    }
    
    public function dashboard(){
        return view('backend.dashboard');
    }
    
    public function  logout(){
        Session::flush();
        return redirect('admin/login')->with('success','Logged Out Successfully');
    }
}

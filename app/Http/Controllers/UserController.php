<?php

namespace App\Http\Controllers;
use App\User;
use App\Record;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    public  function welcome(){
        return view('user.welcome') ;
    }

    public function loginform(){
        return view('user.userLogin');

    }

    public function registration(Request $request){
        $user= new User();
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->user_type = $request['user_type'];
        $user->password = Hash::make(request('password'));
        $user->save();
        return back();
    }
    
    public function login(Request $request){
        if ($request->ismethod('post')) {
            $data = $request->input();
            if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'],'user_type'=>'staff', 'status' => '1'])) {
                return view('user.home');
            } else {
                return back()->with('error',' Invalid UserName Or Password');
            }
        }
    }

    public function userHome()
    {
        return view('user.home');
    }

    public function allRecord()
    {
        $records = Record::paginate(10);
        
        return view('user.home',compact('records'));

    }


    public function searchRecord()
    {
        $parent_id = request('parent_id');
        $subject = request('subject');
        $date = request('date');
        if ( $subject || $date ||$parent_id) {
            $records = Record::where('parent_id', 'LIKE', '%' . $parent_id . '%')
                    ->orWhere('parent_id', $parent_id)
                    ->orWhere('subject', $subject)
                    ->orWhere('date', $date)
                    ->paginate(10);
            return view('user.home', compact('records'));
        }else{
            $records = Record::paginate(10);
            return view('user.home',compact('records'));
        }
    }
}

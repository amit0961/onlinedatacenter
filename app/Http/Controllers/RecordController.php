<?php

namespace App\Http\Controllers;


use App\Record;
use App\User;
use Illuminate\Http\Request;

class RecordController extends Controller
{
    public function index(){
        return view('backend.record.addRecord');
    }

    public function addRecord(Request $request){
        $this->validate($request, [
            'file' => 'required|mimes:pdf,doc,docs,xlx,csv,zip|max:20000',
        ]);
        $records = new Record();
        $records->reference_number = $request['reference_number'];
        if (empty($request['parent_id'])){
            return redirect()->back()->with('error', 'Opps..! Under Receiver Name is missing.');
        }
        $records->parent_id = $request['parent_id'];
        $records->sender = $request['sender'];
        $records->subject = $request['subject'];
        $records->date = $request['date'];
        $fileName = time() . '.' . $request->file->extension();
        $request->file->move(public_path('public/files'), $fileName);
        $records->file = $fileName;
        $records->save();
        return back()->with('success', 'Record Uploaded Successfully!');
    }

    public function viewRecord(){
        $records = Record::all();
        return view('backend.record.viewRecord', compact('records'));
    }

    public function editRecord($id){
        $record = Record::find($id);
        $recordDetails = Record::where(['id' => $id])->first();
        return view('backend.record.editRecord', compact('record', 'recordDetails'));
    }

    public function updateRecord(Request $request, $id){
        $this->validate($request,[
            'file' => 'required|mimes:pdf,doc,docs,xlx,csv,zip|max:20000',
        ]);
        $record = Record::where('id', $id)->first();
        $record->reference_number = $request['reference_number'];
        if (empty($request['parent_id'])){
            return redirect()->back()->with('error', 'Opps...! Under Receiver Name is missing.');
        }
        $record->parent_id = $request['parent_id'];
        $record->sender = $request['sender'];
        $record->subject = $request['subject'];
        $record->date = $request['date'];
        $fileName = time() . '.' . $request->file->extension();
        $request->file->move(public_path('public/files'), $fileName);
        $record->file = $fileName;
        $record->save();
        return back()->with('success', 'Record Updated Successfully!');
    }

    public function deleteRecord($id){
        $record = Record::where('id', $id)->first();
        $record->delete();
        return back()->with('success', 'Record Deleted Successfully!');
    }
}



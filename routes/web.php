<?php

/* Default Section */
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

/* FrontEnd Section start here */
Route::view('user/register', 'auth.user-register')->name('user.register') ;
Route::post('user/registration', 'UserController@registration')->name('user.registration');
Route::get('user/login', 'UserController@loginform')->name('userloginform');
Route::post('user/login', 'UserController@login');
Route::get('/', 'UserController@welcome');
Route::get('/', 'UserController@allRecord');
Route::get('/search-record','UserController@searchRecord')->name('searchRecord');

Route::group(['middleware' => ['UserRole']], function () {
    Route::get('user/home', 'UserController@userHome')->name('user.home');
});
/* FrontEnd Section end here */

/* Admin Section start here */
Route::view('admin/register', 'auth.admin-register');
Route::post('admin/registration', 'AdminController@adminregistration')->name('admin.registration');
Route::get('admin/login', 'AdminController@loginform');
Route::post('admin/dashboard', 'AdminController@adminLogin');

Route::group(['middleware' => ['CheckRole']], function () {
    Route::get('admin/dashboard', 'AdminController@dashboard')->name('admin.dashboard');
    Route::get('admin/addRecord', 'RecordController@index')->name('index');
    Route::post('admin/addRecord', 'RecordController@addRecord')->name('add.record');
    Route::get('admin/viewRecord', 'RecordController@viewRecord')->name('view.record');
    Route::get('admin/{id}/editRecord', 'RecordController@editRecord')->name('edit.record');
    Route::post('admin/{id}/updateRecord', 'RecordController@updateRecord')->name('update.record');
    Route::get('admin/{id}/deleteRecord', 'RecordController@deleteRecord')->name('delete.record');
});
Route::get('/logout', 'AdminController@logout')->name('admin.logout');

/* Admin Section End here */





























